
const http = require("http");

// mock database 
let database = [
    {
        "name" : "brandon",
        "email" : "brandon@mail.com"
    },
    {
        "name" : "jobert",
        "email" : "jobert@mail.com"  
    }
]
http.createServer((req, res) =>{
    if (req.url === "/users" && req.method === "GET"){
        // route forr returning all items upon receiving a get request
        res.writeHead(200, {"Content-Type" : "_application/json"}); // retrieve json
        res.write(JSON.stringify(database)); // convert JSON to string

        // res.write() functions is used to print what is inside the parameters as a response
        // inpuit has to be in a form of string that is why JSON.stringify is used.
        // the data that will be received by the users/client from the server will will be in a form of stringified JSON.
    
        res.end();
    } 
    
    if (req.url === "/users" && req.method === "POST"){
        let requestBody = '';
        /* 
            Data stream - flow/sequence of data 
                Data step - data is received from the client and is processed in the stream called "data" where the code/statements will be triggered 

                End step - only rtuns after the request has completely been sent once the data has already been processed
        */
        req.on("data", function(data){
            // data will be asssigned as the value of the requestBody
            requestBody += data
            console.log(requestBody)

            /* 
            parse - from server
            stringify - to server 
            */
        })
        req.on("end", function(){
            console.log(typeof requestBody)
            requestBody = JSON.parse(requestBody)
            // the server needs an object to arrange information in th edatabase more efficiently that is why we need JSON.parse
            // 
            console.log(typeof requestBody)
            

            let newUser = {
                "name" : requestBody.name,
                "email" : requestBody.email
            }
            database.push(newUser);
            console.log(database);

            res.writeHead(200, {"Content-Type": "_application/json"});
            res.write(JSON.stringify(newUser));
            res.end();
        })
    }
}).listen(4000)

console.log("server is running at local host 4000")

/* 
{
    "name" : "John",
    "email" : "john@email.com"
}
*/



/* 
Activity:
1. In the S27 folder, create an activity folder and an index.js file inside of it.
2. Create a simple server and the following routes with their corresponding HTTP methods and responses:
- If the url is http://localhost:4000/, send a response Welcome to Booking System
- If the url is http://localhost:4000/profile, send a response Welcome to your profile!
- If the url is http://localhost:4000/courses, send a response Here’s our courses available
- If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
- If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
- If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
3. Test all the endpoints in Postman.
4. Create a git repository named S27.
5. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
6. Add the link in Boodle.

*/
