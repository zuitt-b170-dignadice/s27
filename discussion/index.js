/*  
    node.js routing with HTTP methods

    Manage routes using http methods
    peroform create & retrieve operations


    Managing Routes review
    http methods
    create and retrieve operations
    use postman

    the endpoint of our request url can be found as the url property of the request object. 

    request.url

    this is sufficient for retrieving resources from different endpoints. but what about

    create, update, and delete operations?

    HTTP methods
    method - action
    get  - retrieves resources
    post - sends date for creating a resource
    put - sends data for updating a resource
    delete - deletes a specified resource

*/

const http = require("http");
const port = 4000;

const server = http.createServer((req, res) =>{
    // http method of the incoming requests can be accessed via "req.method"
        // GET method - retrieving/reading information; default method
    /*     
    // GET
    if (req.url === "/items" && req.method === "GET"){
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end("Data retrieved from the database")
    } 
    
    */

    /* 
    create an "items" url with POST method
    the response should be "data sent to the database" with 200 as status code and plain text as the contet type

    POST. PUT. DELETE. operation do not work in the browser unlike the get method. with this, postman solves the problem by simulating a frontend for the developers to test their codes.
    */
 
   
    if (req.url === "/items" && req.method === "POST"){
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end("Data to be sent to  the database")
    }
    

    if (req.url === "/items" && req.method === "PUT"){
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end("Upodate resoruces")
    }

    if (req.url === "/items" && req.method === "DELETE"){
        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end("Delete Resources")
    }
})

server.listen(port);

console.log(`Server is running at localhost: ${port}`)